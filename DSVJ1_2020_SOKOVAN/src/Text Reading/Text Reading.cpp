#include "Text Reading.h"
#include <string>

namespace sokovan
{
	namespace text_reading
	{
		const char startReading = '-';
		const char filePathLevel[] = "Resources/Levels/Level Lists.txt";
		void loadText()
		{
			char* textFile = LoadFileText(filePathLevel);
            std::string textFile2(textFile);

            int startingPos = 0;
            int auxLevel = 0;
            while (auxLevel != gameplay::currentLevel)
            {
                if (textFile2[startingPos] == startReading)
                {
                    auxLevel++;
                }
                startingPos++;
            }
            startingPos++;

            gameplay::levelBoxes.clear();

            for (int j = 0; j < TEST_ROWS; j++)
            {
                for (int k = 0; k < TEST_COLUMNS; k++)
                {
                    switch (textFile2[startingPos + k + j * (TEST_COLUMNS + 1)])
                    {
                    case 'F':
                    case 'f':
                        gameplay::blankSlate[j][k]->activate();
                        break;
                    case 'P':
                    case 'p':
                            gameplay::blankSlate[j][k]->activate();
                            gameplay::player1->setPosition(j, k);
                            gameplay::player1->setHitbox(gameplay::blankSlate[j][k]->getHitbox());
                        break;
                    case 'C':
                    case 'c':
                        gameplay::blankSlate[j][k]->activate();
                        gameplay::blankSlate[j][k]->placeBox();
                        gameplay::levelBoxes.push_back(new boxes::boxes(j, k, gameplay::blankSlate[j][k]->getHitbox()));
                        break;
                    case 'D':
                    case 'd':
                        gameplay::blankSlate[j][k]->activate();
                        gameplay::blankSlate[j][k]->destination();
                        break;
                    default:
                        break;
                    }
                }
            }

		}
	}
}