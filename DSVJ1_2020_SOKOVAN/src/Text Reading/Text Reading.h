#ifndef TEXT_READING_H
#define TEXT_READING_H

#include "raylib.h"
#include "Scenes/Gameplay/Gameplay.h"

namespace sokovan
{
	namespace text_reading
	{
		void loadText();
	}
}

#endif // !TEXT_READING_H

