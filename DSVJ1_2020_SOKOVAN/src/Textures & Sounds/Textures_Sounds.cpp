#include "Textures_Sounds.h"

namespace sokovan
{
	namespace textures
	{
		//Main Menu Textures
		Texture2D logo;
		Texture2D backGround;
		Texture2D play;
		Texture2D settings;
		Texture2D exit;

		//Gameplay Textures
		Texture2D backGroundGameplay;
		Texture2D movementBar;
		Texture2D player;
		Texture2D floor;
		Texture2D box;

		//Game End Textures
		Texture2D victory;
		Texture2D backToMenu;

		//Main Menu Textures
		Texture2D backgroundSettings;
		Texture2D audio_sfx;
		Texture2D credits;
		Texture2D back;

		//Pause Textures
		Texture2D returnToGame;
		Texture2D mainMenu;

		//Credits Textures
		Texture2D creditsIcon;
		Texture2D returnToMainMenu;

		void initMainMenu()
		{
			logo = LoadTexture("Resources/Assets/Logo.png");
			backGround = LoadTexture("Resources/Assets/Menu Background.png");
			play = LoadTexture("Resources/Assets/Play .png");
			settings = LoadTexture("Resources/Assets/Settings.png");
			exit = LoadTexture("Resources/Assets/Back.png");
		}

		void deInitMainMenu()
		{
			UnloadTexture(logo);
			UnloadTexture(backGround);
			UnloadTexture(play);
			UnloadTexture(settings);
			UnloadTexture(exit);
		}

		void initGameplay()
		{
			backGroundGameplay = LoadTexture("Resources/Assets/Gameplay Background.png");
			movementBar = LoadTexture("Resources/Assets/Control Bar.png");
			player = LoadTexture("Resources/Assets/Character Sheet.png");
			floor = LoadTexture("Resources/Assets/Floor.png");
			box = LoadTexture("Resources/Assets/Box.png");
		}

		void deInitGameplay()
		{
			UnloadTexture(movementBar);
			UnloadTexture(player);
			UnloadTexture(floor);
			UnloadTexture(box);
		}

		void initVictory()
		{
			victory = LoadTexture("Resources/Assets/Victory.png");
			backToMenu = LoadTexture("Resources/Assets/Main Menu.png");
		}

		void deInitVictory()
		{
			UnloadTexture(victory);
			UnloadTexture(backToMenu);
		}

		void initSettings()
		{
			backgroundSettings = LoadTexture("Resources/Assets/Menu BackGround.png");
			audio_sfx = LoadTexture("Resources/Assets/Audio.png");
			credits = LoadTexture("Resources/Assets/Credits.png");
			back = LoadTexture("Resources/Assets/Back.png");
		}

		void deInitSettings()
		{
			UnloadTexture(backgroundSettings);
			UnloadTexture(audio_sfx);
			UnloadTexture(credits);
			UnloadTexture(back);
		}

		void initPause()
		{
			returnToGame = LoadTexture("Resources/Assets/Back.png");
			mainMenu = LoadTexture("Resources/Assets/Main Menu.png");
		}

		void deInitPause()
		{
			UnloadTexture(returnToGame);
			UnloadTexture(mainMenu);
		}

		void initCredits()
		{
			creditsIcon = LoadTexture("Resources/Assets/Credits Image.png");
			returnToMainMenu = LoadTexture("Resources/Assets/Back.png");
		}

		void deInitCredits()
		{
			UnloadTexture(credits);
			UnloadTexture(returnToMainMenu);
		}
	}
	namespace sounds
	{
		Music mainMenu;
		Music gamePlay;
		float musicVolume;

		void initSounds()
		{
			mainMenu = LoadMusicStream("Resources/Music/Main Menu Music.mp3");
			gamePlay = LoadMusicStream("Resources/Music/Gameplay Music.mp3");
		}

		void deInitSounds()
		{
			UnloadMusicStream(mainMenu);
			UnloadMusicStream(gamePlay);
		}
	}
}