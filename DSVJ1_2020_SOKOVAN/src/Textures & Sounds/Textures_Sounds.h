#ifndef TEXTURES_SOUNDS_H
#define TEXTURES_SOUNDS_H

#include "raylib.h"

namespace sokovan
{
	namespace textures
	{
		//Main menu textures
		extern Texture2D logo;
		extern Texture2D play;
		extern Texture2D settings;
		extern Texture2D exit;
		extern Texture2D backGround;

		//Gameplay Textures
		extern Texture2D backGroundGameplay;
		extern Texture2D movementBar;
		extern Texture2D player;
		extern Texture2D floor;
		extern Texture2D box;

		//Setting Textures
		extern Texture2D backgroundSettings;
		extern Texture2D audio_sfx;
		extern Texture2D credits;
		extern Texture2D back;

		//Pause Textures
		extern Texture2D returnToGame;
		extern Texture2D mainMenu;

		//Game End Textures
		extern Texture2D victory;
		extern Texture2D backToMenu;

		//Credits Textures
		extern Texture2D creditsIcon;
		extern Texture2D returnToMainMenu;

		void initMainMenu();
		void deInitMainMenu();

		void initGameplay();
		void deInitGameplay();

		void initSettings();
		void deInitSettings();

		void initVictory();
		void deInitVictory();

		void initPause();
		void deInitPause();

		void initCredits();
		void deInitCredits();
	}
	namespace sounds
	{
		extern float musicVolume;

		extern Music mainMenu;
		extern Music gamePlay;

		void initSounds();
		void deInitSounds();
	}
}

#endif
