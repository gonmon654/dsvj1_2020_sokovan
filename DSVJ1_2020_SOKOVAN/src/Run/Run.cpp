#include "Run.h"

namespace sokovan
{
	namespace run
	{
		status current;
		status newStatus;

		void init()
		{
			switch (current)
			{
			case status::MAIN_MENU:
				textures::initMainMenu();
				main_menu::init();
				break;
			case status::IN_GAME:
				textures::initGameplay();
				gameplay::init();
				break;
			case status::SETTINGS:
				textures::initSettings();
				settings::init();
				break;
			case status::GAME_PAUSE:
				textures::initPause();
				pause::init();
				break;
			case status::CREDITS:
				textures::initCredits();
				credits::init();
				break;
			case status::VICTORY_SCREEN:
				textures::initVictory();
				victory_screen::init();
				break;
			}
		}

		void update()
		{
			switch (current)
			{
			case status::MAIN_MENU:
				if (!IsMusicPlaying(sounds::mainMenu))
				{
					PlayMusicStream(sounds::mainMenu);
				}
				else
				{
					UpdateMusicStream(sounds::mainMenu);
				}
				main_menu::update();
				break;
			case status::IN_GAME:
				if (!IsMusicPlaying(sounds::gamePlay))
				{
					PlayMusicStream(sounds::gamePlay);
				}
				else
				{
					UpdateMusicStream(sounds::gamePlay);
				}
				gameplay::update();
				break;
			case status::SETTINGS:
				if (!IsMusicPlaying(sounds::mainMenu))
				{
					PlayMusicStream(sounds::mainMenu);
				}
				else
				{
					UpdateMusicStream(sounds::mainMenu);
				}
				settings::update();
				break;
			case status::GAME_PAUSE:
				pause::update();
				break;
			case status::CREDITS:
				credits::update();
				break;
			case status::VICTORY_SCREEN:
				victory_screen::update();
				break;
			}
		}

		void draw()
		{
			BeginDrawing();
			ClearBackground(WHITE);
			switch (current)
			{
			case status::MAIN_MENU:
				main_menu::draw();
				break;
			case status::IN_GAME:
				gameplay::draw();
				break;
			case status::SETTINGS:
				settings::draw();
				break;
			case status::GAME_PAUSE:
				pause::draw();
				break;
			case status::CREDITS:
				credits::draw();
				break;
			case status::VICTORY_SCREEN:
				victory_screen::draw();
				break;
			}
			EndDrawing();
		}

		void deInit()
		{
			switch (current)
			{
			case status::MAIN_MENU:
				textures::deInitMainMenu();
				main_menu::deInit();
				break;
			case status::IN_GAME:
				textures::deInitGameplay();
				gameplay::deInit();
				break;
			case status::SETTINGS:
				textures::deInitSettings();
				settings::deInit();
				break;
			case status::GAME_PAUSE:
				textures::deInitPause();
				pause::deInit();
				break;
			case status::CREDITS:
				textures::deInitCredits();
				credits::deInit();
				break;
			case status::VICTORY_SCREEN:
				textures::deInitVictory();
				break;
			}
		}

		void changeStatus()
		{
			bool arrivedFromPause = false;

			switch (newStatus)
			{
			case status::MAIN_MENU:
				if (current == status::SETTINGS)
				{
					textures::deInitSettings();
					settings::deInit();
				}
				if (current == status::GAME_PAUSE)
				{
					pause::deInit();
					gameplay::deInit();
					textures::deInitGameplay();
				}
				if (current == status::IN_GAME)
				{
					gameplay::deInit();
					textures::deInitGameplay();
				}
				if (current == status::VICTORY_SCREEN)
				{
					textures::deInitVictory();
					gameplay::deInit();
					textures::deInitGameplay();
				}
				current = newStatus;
				break;
			case status::IN_GAME:
				if (current == status::GAME_PAUSE)
				{
					textures::deInitPause();
					pause::deInit();
					arrivedFromPause = true;
				}
				if (current == status::MAIN_MENU)
				{
					main_menu::deInit();
				}
				current = newStatus;
				break;
			case status::SETTINGS:
				if (current == status::MAIN_MENU)
				{
					main_menu::deInit();
				}
				current = newStatus;
				break;
			case status::GAME_PAUSE:
				current = newStatus;
				break;
			case status::CREDITS:
				settings::deInit();
				current = newStatus;
				break;
			case status::VICTORY_SCREEN:
				current = newStatus;
				break;
			}

			switch (current)
			{
			case status::MAIN_MENU:
				current = newStatus;
				main_menu::init();
				break;
			case status::IN_GAME:
				textures::initGameplay();
				gameplay::init();
				break;
			case status::SETTINGS:
				textures::initSettings();
				settings::init();
				break;
			case status::GAME_PAUSE:
				textures::initPause();
				pause::init();
				break;
			case status::CREDITS:
				textures::initCredits();
				credits::init();
				break;
			case status::VICTORY_SCREEN:
				textures::initVictory();
				victory_screen::init();
				break;
			}
		}

		void run()
		{
			SetExitKey(KEY_F12);
			InitWindow(screen_size::screenWidth, screen_size::screenHeight, "Sokoban - V1.0");
			InitAudioDevice();
			SetTargetFPS(60);
			sounds::musicVolume = 0.5;
			current = status::MAIN_MENU;
			newStatus = status::MAIN_MENU;

			sounds::initSounds();
			sokovan::run::init();
			while (current != status::EXIT && !WindowShouldClose())
			{
				update();
				draw();
			}
			deInit();
			sounds::deInitSounds();
		}
	}
}