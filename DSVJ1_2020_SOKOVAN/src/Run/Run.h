#ifndef RUN_H
#define RUN_H

#include "raylib.h"

#include "Textures & Sounds/Textures_Sounds.h"
#include "Screen Size/Screen Size.h"

#include "Scenes/Main Menu/Main Menu.h"
#include "Scenes/Settings/Settings.h"
#include "Scenes/Audio/Audio.h"
#include "Scenes/Game Over/Game Over.h"
#include "Scenes/Pause/Pause.h"
#include "Scenes/Gameplay/Gameplay.h"
#include "Scenes/Credits/Credits.h"
#include "Scenes/Victory Screen/Victory Screen.h"


namespace sokovan
{
	namespace run
	{
		enum class status { MAIN_MENU = 1, IN_GAME, SETTINGS, GAME_PAUSE, EXIT, CREDITS,VICTORY_SCREEN };

		extern status current;
		extern status newStatus;

		void init();
		void update();
		void draw();
		void deInit();
		void changeStatus();
		void run();
	}
}

#endif // !RUN_H

