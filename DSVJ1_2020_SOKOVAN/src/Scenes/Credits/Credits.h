#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"

#include "Screen Size/Screen Size.h"
#include "Run/Run.h"
#include "Textures & Sounds/Textures_Sounds.h"

namespace sokovan
{
	namespace credits
	{
		extern Rectangle hitbox;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif
