#include "Credits.h"

namespace sokovan
{
	namespace credits
	{
		Rectangle hitbox;

		void init()
		{
			float width = static_cast<float>(screen_size::screenWidth) / 4;
			float height = static_cast<float>((screen_size::screenHeight / 6));
			hitbox = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)(screen_size::screenHeight / 6) * 5, width, height };

			//Setting Texture Sizes
			textures::returnToMainMenu.width = (int)width;
			textures::returnToMainMenu.height = (int)height;

			textures::creditsIcon.width = (int)screen_size::screenWidth;
			textures::creditsIcon.height = (int)((screen_size::screenHeight / 6) * 5);
		}

		void update()
		{
			if (CheckCollisionPointRec(GetMousePosition(), hitbox))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					run::newStatus = run::status::SETTINGS;
					run::changeStatus();
				}
			}

		}

		void draw()
		{
			DrawTexture(textures::creditsIcon, 0, 0, WHITE);
			DrawTexture(textures::returnToMainMenu, (int)hitbox.x, (int)hitbox.y, WHITE);
		}

		void deInit()
		{

		}
	}
}