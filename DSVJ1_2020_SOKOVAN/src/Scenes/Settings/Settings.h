#ifndef SETTINGS_H
#define SETTINGS_H

#include "Run/Run.h"
#include "Screen Size/Screen Size.h"
#include "raylib.h"

namespace sokovan
{
	namespace settings
	{
		extern Rectangle _hitbox[3];
		extern Rectangle sourceRec;
		extern bool audioOn;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif
