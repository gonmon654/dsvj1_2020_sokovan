#include "Settings.h"

namespace sokovan
{
	namespace settings
	{
		Rectangle _hitbox[3];
		bool audioOn = true;
		Rectangle sourceRec;

		void init()
		{
			const float width = static_cast<float>((screen_size::screenWidth / 4));
			_hitbox[0] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 2),width,(float)((screen_size::screenHeight / 7) * 1) };
			_hitbox[1] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 3),width,(float)((screen_size::screenHeight / 7) * 1) };
			_hitbox[2] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 4),width,(float)((screen_size::screenHeight / 7) * 1) };

			textures::backgroundSettings.height = screen_size::screenHeight;
			textures::backgroundSettings.width = screen_size::screenWidth;

			textures::back.height = static_cast<int>(_hitbox[2].height);
			textures::back.width = static_cast<int>(_hitbox[2].width);

			textures::credits.height = static_cast<int>(_hitbox[1].height);
			textures::credits.width = static_cast<int>(_hitbox[1].width);

			sourceRec.height = (float)textures::audio_sfx.height / 2;
			sourceRec.width = (float)textures::audio_sfx.width;

			if (audioOn)
			{
				sourceRec.y = 0;
			}
			else
			{
				sourceRec.y = (float)(textures::audio_sfx.height / 2);
			}
		}

		void update()
		{
			for (short i = 0; i < 3; i++)
			{
				if (i == 0)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (audioOn == true)
							{
								SetMusicVolume(sounds::mainMenu, 0.0f);
								SetMusicVolume(sounds::gamePlay, 0.0f);
								audioOn = false;
								sourceRec.y += textures::audio_sfx.height / 2;
							}
							else
							{
								SetMusicVolume(sounds::mainMenu, 0.5f);
								SetMusicVolume(sounds::gamePlay, 0.5f);
								audioOn = true;
								sourceRec.y = 0;
							}
						}
					}
				}
				if (i == 1)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::CREDITS;
							run::changeStatus();
						}
					}
				}
				if (i == 2)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::MAIN_MENU;
							run::changeStatus();
						}
					}
				}
			}
		}

		void draw()
		{
			if (audioOn)
			{
				DrawTexture(textures::backgroundSettings, 0, 0, WHITE);
				DrawTexturePro(textures::audio_sfx, sourceRec, _hitbox[0], { 0,0 }, 0, WHITE);
				DrawTexture(textures::credits, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
				DrawTexture(textures::back, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);
			}
			else
			{
				DrawTexture(textures::backgroundSettings, 0, 0, WHITE);
				DrawTexturePro(textures::audio_sfx, sourceRec, _hitbox[0], { 0,0 }, 0, WHITE);
				DrawTexture(textures::credits, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
				DrawTexture(textures::back, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);

			}
		}

		void deInit()
		{

		}
	}
}