#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "Run/Run.h"
#include "Screen Size/Screen Size.h"
#include "raylib.h"

namespace sokovan
{
	namespace main_menu
	{
		enum class selected { PLAY = 1, SETTINGS, EXIT, NONE };

		extern Rectangle _hitbox[3];
		extern selected _current;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif
