#include "Main Menu.h"
#include "Scenes/Gameplay/Gameplay.h"

namespace sokovan
{
	namespace main_menu
	{
		Rectangle _hitbox[3];
		selected _current = selected::NONE;

		void init()
		{
			const float height = static_cast<float>(((screen_size::screenHeight / 10) * 1));
			const float width = static_cast<float>(screen_size::screenWidth / 4);
			_hitbox[0] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 2),(float)width,(float)((screen_size::screenHeight / 10) * 1) };
			_hitbox[1] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 3),(float)(width),(float)((screen_size::screenHeight / 10) * 1) };
			_hitbox[2] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 4),(float)width,(float)((screen_size::screenHeight / 10) * 1) };

			textures::logo.width = (int)(width * 1.5);
			textures::logo.height = (int)(height * 2.0);

			textures::play.width = static_cast<int>(_hitbox[0].width);
			textures::play.height = static_cast<int>(_hitbox[0].height);

			textures::settings.width = static_cast<int>(_hitbox[1].width);
			textures::settings.height = static_cast<int>(_hitbox[1].height);

			textures::exit.width = static_cast<int>(_hitbox[2].width);
			textures::exit.height = static_cast<int>(_hitbox[2].height);

			textures::backGround.width = screen_size::screenWidth;
			textures::backGround.height = screen_size::screenHeight;

		}

		void update()
		{
			for (short i = 0; i < 3; i++)
			{
				if (i == 0)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						_current = main_menu::selected::PLAY;
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::IN_GAME;
							run::changeStatus();
						}
					}
				}
				if (i == 1)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						_current = main_menu::selected::SETTINGS;
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::SETTINGS;
							run::changeStatus();
						}
					}
				}
				if (i == 2)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						_current = main_menu::selected::EXIT;
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::EXIT;
							run::changeStatus();
						}
					}
				}
			}
		}

		void draw()
		{
			switch (_current)
			{
			case selected::NONE:
					DrawTexture(textures::backGround, 0, 0, WHITE);
					DrawTexture(textures::logo, (int)((screen_size::screenWidth / 2) - (textures::logo.width / 2)), 0, WHITE);
					DrawTexture(textures::play, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
					DrawTexture(textures::settings, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
					DrawTexture(textures::exit, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);
				break;
			case selected::PLAY:
					DrawTexture(textures::backGround, 0, 0, WHITE);
					DrawTexture(textures::logo, (int)((screen_size::screenWidth / 2) - (textures::logo.width / 2)), 0, WHITE);
					DrawTexture(textures::play, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
					DrawTexture(textures::settings, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
					DrawTexture(textures::exit, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);			
				break;
			case selected::SETTINGS:
					DrawTexture(textures::backGround, 0, 0, WHITE);
					DrawTexture(textures::logo, (int)((screen_size::screenWidth / 2) - (textures::logo.width / 2)), 0, WHITE);
					DrawTexture(textures::play, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
					DrawTexture(textures::settings, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
					DrawTexture(textures::exit, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);
				break;
			case selected::EXIT:
					DrawTexture(textures::backGround, 0, 0, WHITE);
					DrawTexture(textures::logo, (int)((screen_size::screenWidth / 2) - (textures::logo.width / 2)), 0, WHITE);
					DrawTexture(textures::play, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
					DrawTexture(textures::settings, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
					DrawTexture(textures::exit, (int)_hitbox[2].x, (int)_hitbox[2].y, WHITE);
				break;
			}
		}

		void deInit()
		{

		}
	}
}