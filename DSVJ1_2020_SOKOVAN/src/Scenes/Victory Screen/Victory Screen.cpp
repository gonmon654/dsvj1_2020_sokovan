#include "Victory Screen.h"
#include "Screen Size/Screen Size.h"
#include "Textures & Sounds/Textures_Sounds.h"
namespace sokovan
{
	namespace victory_screen
	{
		Rectangle textBox[2];
		void init()
		{
			const float height = static_cast<float>(((screen_size::screenHeight / 10) * 1));
			const float width = static_cast<float>(screen_size::screenWidth / 4);

			textBox[0] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 2),(float)width,(float)height };
			textBox[1] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 4),(float)width,(float)height };

			textures::victory.width = (int)textBox[0].width;
			textures::victory.height = (int)textBox[0].height;

			textures::backToMenu.width = (int)textBox[1].width;
			textures::backToMenu.height = (int)textBox[1].height;
		}
		void update()
		{
			if(CheckCollisionPointRec(GetMousePosition(), textBox[1]))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					run::newStatus = run::status::MAIN_MENU;
					run::changeStatus();
					return;
				}
			}
		}
		void draw()
		{
			DrawTexture(textures::backGroundGameplay, 0, 0, WHITE);
			DrawTexture(textures::victory, (int)textBox[0].x, (int)textBox[0].y,WHITE);
			DrawTexture(textures::backToMenu, (int)textBox[1].x, (int)textBox[1].y, WHITE);
		}
	}
}