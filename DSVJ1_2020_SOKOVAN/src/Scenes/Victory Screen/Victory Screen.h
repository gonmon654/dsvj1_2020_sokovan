#ifndef VICTORY_SCREEN_H
#define VICTORY_SCREEN_H
#include "Run/Run.h"
#include "raylib.h"
namespace sokovan
{
	namespace victory_screen
	{
		extern Rectangle textBox[2];
		void init();
		void update();
		void draw();
	}
}
#endif // !VICTORY_SCREEN_H
