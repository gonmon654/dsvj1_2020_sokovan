#include "Gameplay.h"
#include "Text Reading/Text Reading.h"

namespace sokovan
{
	namespace gameplay
	{
		player::player* player1;
		tiles::tiles* blankSlate[TEST_ROWS][TEST_COLUMNS];
		Rectangle hitbox;
		std::vector<boxes::boxes*> levelBoxes;
		int currentLevel = 1;
		float width = static_cast<float> (screen_size::screenWidth / TEST_COLUMNS);
		float height = static_cast<float>((((screen_size::screenHeight / 4) * 2) / TEST_ROWS));

		bool checkLevelWin()
		{
			int size = levelBoxes.size();
			for (short i = 0; i < size; i++)
			{
				if (!blankSlate[(int)levelBoxes[i]->getPosition().y][(int)levelBoxes[i]->getPosition().x]->getDestination())
				{
					return false;
				}
			}
			return true;
		}

		void init()
		{
			currentLevel = 1;
			player1 = new player::player();
			for (short i = 0; i < TEST_ROWS; i++)
			{
				for (short j = 0; j < TEST_COLUMNS; j++)
				{
					blankSlate[i][j] = new tiles::tiles();
					blankSlate[i][j]->setHitbox({ (j * width),((screen_size::screenHeight / 4) + (i * height)),(width),(height) });
				}
			}
			text_reading::loadText();

			//Texture Sizes
			textures::floor.width = (int)width;
			textures::floor.height = (int)height;

			textures::box.width = (int)width;
			textures::box.height = (int)height;

			textures::movementBar.width = screen_size::screenWidth;
			textures::movementBar.height = (screen_size::screenHeight / 4);

			textures::backGroundGameplay.width = screen_size::screenWidth;
			textures::backGroundGameplay.height = screen_size::screenHeight;

		}

		void update()
		{
			if (IsKeyPressed(KEY_P))
			{
				run::newStatus = run::status::GAME_PAUSE;
				run::changeStatus();
			}
			if (IsKeyPressed(KEY_R))
			{
				for (short i = 0; i < (int)levelBoxes.size(); i++)
				{
					delete levelBoxes[i];
				}
				text_reading::loadText();
			}
#if DEBUG
			{
				if (IsKeyPressed(KEY_F1))
				{
					currentLevel++;
					if (currentLevel > 5)
					{
						for (short i = 0; i < (int)levelBoxes.size(); i++)
						{
							delete levelBoxes[i];
						}
						for (short i = 0; i < TEST_ROWS; i++)
						{
							for (short j = 0; j < TEST_COLUMNS; j++)
							{
								delete blankSlate[i][j];
							}
						}
						run::newStatus = run::status::VICTORY_SCREEN;
						run::changeStatus();
						return;
					}
					else
					{
						for (short i = 0; i < (int)levelBoxes.size(); i++)
						{
							delete levelBoxes[i];
						}

						for (short i = 0; i < TEST_ROWS; i++)
						{
							for (short j = 0; j < TEST_COLUMNS; j++)
							{
								delete blankSlate[i][j];
								blankSlate[i][j] = new tiles::tiles();
								blankSlate[i][j]->setHitbox({ (j * width),((screen_size::screenHeight / 4) + (i * height)),(width),(height) });
							}
						}
						delete player1;
						player1 = new player::player();
						text_reading::loadText();
					}
				}
			}
#endif
				player1->update();
				for (short i = 0; i < (int)levelBoxes.size(); i++)
				{
					levelBoxes[i]->update();
				}
			if (checkLevelWin())
			{
				currentLevel++;
				if (currentLevel > 5)
				{
					for (short i = 0; i < (int)levelBoxes.size(); i++)
					{
						delete levelBoxes[i];
					}
					run::newStatus = run::status::VICTORY_SCREEN;
					run::changeStatus();
					return;
				}
				else
				{
					for (short i = 0; i < (int)levelBoxes.size(); i++)
					{
						delete levelBoxes[i];
					}
					for (short i = 0; i < TEST_ROWS; i++)
					{
						for (short j = 0; j < TEST_COLUMNS; j++)
						{
							delete blankSlate[i][j];
							blankSlate[i][j] = new tiles::tiles();
							blankSlate[i][j]->setHitbox({ (j * width),((screen_size::screenHeight / 4) + (i * height)),(width),(height) });
						}
					}
					delete player1;
					player1 = new player::player();
					text_reading::loadText();
				}
			}
		}

		void draw()
		{
			DrawTexture(textures::backGroundGameplay, 0, 0, WHITE);
			DrawTexture(textures::movementBar, 0, 0, WHITE);

			for (short i = 0; i < TEST_ROWS; i++)
			{
				for (short j = 0; j < TEST_COLUMNS; j++)
				{
					if (blankSlate[i][j]->getActive())
					{
						blankSlate[i][j]->draw();						
					}
				}
			}
			player1->draw();
			int size = levelBoxes.size();
			for (short i = 0; i < size; i++)
			{
				levelBoxes[i]->draw();
			}
		}

		void deInit()
		{
			if (player1 != NULL)
			{
				delete player1;
				player1 = NULL;
			}
		}
	}
}
