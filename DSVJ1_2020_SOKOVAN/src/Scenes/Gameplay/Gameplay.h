#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include <vector>

#include "Objects/Player/Player.h"
#include "Objects/Blocks/Boxes.h"
#include "Objects/Tiles/Tiles.h"

#include "Run/Run.h"

#include "Screen Size/Screen Size.h"
#include "raylib.h"

#define TEST_ROWS 8
#define TEST_COLUMNS 15

namespace sokovan
{
	namespace gameplay
	{
		extern float width;
		extern float height;

		extern player::player* player1;
		extern tiles::tiles* blankSlate[TEST_ROWS][TEST_COLUMNS];
		extern std::vector<boxes::boxes*> levelBoxes;

		extern int currentLevel;
		
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif
