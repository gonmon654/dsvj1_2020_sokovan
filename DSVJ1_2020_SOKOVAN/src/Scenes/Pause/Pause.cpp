#include "Pause.h"

namespace sokovan
{
	namespace pause
	{
		Rectangle _hitbox[2];
		selected _current;

		void init()
		{
			const float height = static_cast<float>(((screen_size::screenHeight / 10) * 1));
			const float width = static_cast<float>(screen_size::screenWidth / 4);

			_hitbox[0] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 2),(float)width,(float)height };
			_hitbox[1] = { (float)((screen_size::screenWidth / 2) - (width / 2)),(float)((screen_size::screenHeight / 5) * 4),(float)width,(float)height };

			textures::returnToGame.width = (int)_hitbox[0].width;
			textures::returnToGame.height = (int)_hitbox[0].height;

			textures::mainMenu.width = (int)_hitbox[1].width;
			textures::mainMenu.height = (int)_hitbox[1].height;
		}

		void update()
		{
			for (short i = 0; i < 3; i++)
			{
				if (i == 0)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						_current = pause::selected::RESUME;
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::IN_GAME;
							run::changeStatus();
						}
					}
				}
				if (i == 1)
				{
					if (CheckCollisionPointRec(GetMousePosition(), _hitbox[i]))
					{
						_current = pause::selected::EXIT;
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							run::newStatus = run::status::MAIN_MENU;
							run::changeStatus();
						}
					}
				}
			}
		}

		void draw()
		{
			switch (_current)
			{
			case selected::NONE:
				DrawTexture(textures::returnToGame, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
				DrawTexture(textures::mainMenu, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
				break;
			case selected::RESUME:
				DrawTexture(textures::returnToGame, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
				DrawTexture(textures::mainMenu, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
				break;
			case selected::EXIT:
				DrawTexture(textures::returnToGame, (int)_hitbox[0].x, (int)_hitbox[0].y, WHITE);
				DrawTexture(textures::mainMenu, (int)_hitbox[1].x, (int)_hitbox[1].y, WHITE);
				break;
			}
		}

		void deInit()
		{

		}
	}
}