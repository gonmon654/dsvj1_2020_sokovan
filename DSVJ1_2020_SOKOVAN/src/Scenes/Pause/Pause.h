#ifndef PAUSE_H
#define PAUSE_H

#include "raylib.h"

#include "Run/Run.h"
#include "Textures & Sounds/Textures_Sounds.h"
#include "Screen Size/Screen Size.h"

namespace sokovan
{
	namespace pause
	{
		enum class selected { RESUME = 1, EXIT, NONE };

		extern Rectangle _hitbox[2];
		extern selected _current;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif
