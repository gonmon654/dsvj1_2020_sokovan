#ifndef BLOCKS_H
#define BLOCKS_H

#include "raylib.h"

namespace sokovan
{
	namespace boxes
	{
		enum class state { MOVING_UP, MOVING_DOWN,MOVING_LEFT, MOVING_RIGHT,NONE };

		class boxes
		{
		private:
			Vector2 position;
			Rectangle _hitbox;
			bool _moving;
			Vector2 target;
			state _current;
		public:
			boxes(int y, int x, Rectangle newHitbox);
			void update();
			void draw();
			void setHitbox(Rectangle newHitbox);
			void setPosition(Vector2 newPosition);
			void setState(state newState);
			Vector2 getPosition();
			void setTarget(Rectangle newTargets);
		};
	}
}

#endif
