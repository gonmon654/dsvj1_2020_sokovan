#include "Boxes.h"
#include "Scenes/Gameplay/Gameplay.h"

namespace sokovan
{
	namespace boxes
	{
		const int moveSpeed = 100;

		boxes::boxes(int y, int x, Rectangle newHitbox)
		{
			position = { (float)x,(float)y };
			_hitbox = newHitbox;
			_moving = false;
			target = { 0,0 };
			_current = state::NONE;
		}

		void boxes::update()
		{
			if (_moving)
			{
				switch (_current)
				{
				case sokovan::boxes::state::MOVING_UP:
					_hitbox.y -= moveSpeed * GetFrameTime();
					if (_hitbox.y < target.y)
					{
						_hitbox.y = target.y;
						_current = state::NONE;
						_moving = false;
					}
					break;
				case sokovan::boxes::state::MOVING_DOWN:
					_hitbox.y += moveSpeed * GetFrameTime();
					if (_hitbox.y > target.y)
					{
						_hitbox.y = target.y;
						_current = state::NONE;
						_moving = false;
					}
					break;
				case sokovan::boxes::state::MOVING_LEFT:
					_hitbox.x -= moveSpeed * GetFrameTime();
					if (_hitbox.x < target.x)
					{
						_hitbox.x = target.x;
						_current = state::NONE;
						_moving = false;
					}
					break;
				case sokovan::boxes::state::MOVING_RIGHT:
					_hitbox.x += moveSpeed * GetFrameTime();
					if (_hitbox.x > target.x)
					{
						_hitbox.x = target.x;
						_current = state::NONE;
						_moving = false;
					}
					break;
				case sokovan::boxes::state::NONE:
					break;
				default:
					break;
				}
			}
		}

		void boxes::draw()
		{
			DrawTexture(textures::box, (int)_hitbox.x, (int)_hitbox.y, WHITE);
		}

		void boxes::setHitbox(Rectangle newHitbox)
		{
			_hitbox = newHitbox;
		}

		void boxes::setPosition(Vector2 newPosition)
		{
			position = newPosition;
		}

		Vector2 boxes::getPosition()
		{
			return position;
		}

		void boxes::setState(state newState)
		{
			_current = newState;
		}

		void boxes::setTarget(Rectangle newTargets)
		{
			target.x = newTargets.x;
			target.y = newTargets.y;
			_moving = true;
		}
	}
}