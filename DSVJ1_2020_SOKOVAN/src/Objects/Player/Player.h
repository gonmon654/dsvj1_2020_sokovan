#ifndef PLAYER_H
#define PLAYER_H

#include "Textures & Sounds/Textures_Sounds.h"
#include "raylib.h"

namespace sokovan
{
	namespace player
	{
		enum class state { IDLE_UP, WALKING_UP, IDLE_DOWN, WALKING_DOWN, IDLE_LEFT, WALKING_LEFT, IDLE_RIGHT, WALKING_RIGHT };
		class player
		{
		private:
			Vector2 position;
			Rectangle _hitbox;
			Rectangle _sourceRec;
			int _currentLevel;
			int up;
			int down;
			int left;
			int right;
			state _currentState;
			void textureUpdate();
			void switchState(state newState);
			bool _moving;
			Vector2 target;
		public:
			player();
			void update();
			void draw();
			Vector2 getPosition();
			void setHitbox(Rectangle hitbox);
			void setPosition(int y, int x);
		};
	}
}
#endif
