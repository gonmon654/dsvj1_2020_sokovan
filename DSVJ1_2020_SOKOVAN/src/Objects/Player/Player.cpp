#include "Player.h"
#include "Scenes/Gameplay/Gameplay.h"

namespace sokovan
{
	namespace player
	{
		const int animFrames = 30;
		const int diffAnims = 8;
		const int moveSpeed = 100;

		player::player()
		{
			position.x = TEST_COLUMNS / 2;
			position.y = TEST_ROWS / 2;
			_currentLevel = 1;
			up = KEY_W;
			down = KEY_S;
			left = KEY_A;
			right = KEY_D;
			_hitbox = { 0,0,0,0 };
			_sourceRec = { 0,(float)((textures::player.height / diffAnims) * 3),(float)(textures::player.width / animFrames),(float)(textures::player.height / diffAnims) };
			switchState(state::IDLE_DOWN);
			_moving = false;
			target = { 0,0 };
		}

		void player::textureUpdate()
		{
			_sourceRec.x += (float)textures::player.width / animFrames;
			if (_sourceRec.x > textures::player.width)
			{
				_sourceRec.x = 0;
			}
		}

		void player::switchState(state newState)
		{
			if (_currentState == newState) { return; }
			_currentState = newState;
			_sourceRec.x = 0;
			switch (_currentState)
			{
			case sokovan::player::state::IDLE_UP:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 0);
				break;
			case sokovan::player::state::WALKING_UP:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 1);
				break;
			case sokovan::player::state::IDLE_DOWN:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 2);
				break;
			case sokovan::player::state::WALKING_DOWN:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 3);
				break;
			case sokovan::player::state::IDLE_LEFT:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 4);
				break;
			case sokovan::player::state::WALKING_LEFT:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 5);
				break;
			case sokovan::player::state::IDLE_RIGHT:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 6);
				break;
			case sokovan::player::state::WALKING_RIGHT:
				_sourceRec.y = (float)((textures::player.height / diffAnims) * 7);
				break;
			default:
				break;
			}
		}

		void player::update()
		{
			textureUpdate();
			if (_moving)
			{
				switch (_currentState)
				{
				case sokovan::player::state::WALKING_UP:
					_hitbox.y -= moveSpeed * GetFrameTime();
					if (_hitbox.y < target.y)
					{
						_hitbox.y = target.y;
						switchState(state::IDLE_UP);
						_moving = false;
					}
					break;
				case sokovan::player::state::WALKING_DOWN:
					_hitbox.y += moveSpeed * GetFrameTime();
					if (_hitbox.y > target.y)
					{
						_hitbox.y = target.y;
						switchState(state::IDLE_DOWN);
						_moving = false;
					}
					break;
				case sokovan::player::state::WALKING_LEFT:
					_hitbox.x -= moveSpeed * GetFrameTime();
					if (_hitbox.x < target.x)
					{
						_hitbox.x = target.x;
						switchState(state::IDLE_LEFT);
						_moving = false;
					}
					break;
				case sokovan::player::state::WALKING_RIGHT:
					_hitbox.x += moveSpeed * GetFrameTime();
					if (_hitbox.x > target.x)
					{
						_hitbox.x = target.x;
						switchState(state::IDLE_RIGHT);
						_moving = false;
					}
					break;
				default:
					break;
				}
			}
			else
			{
				Vector2 locatorCharacter; //Will be used for regular movement
				Vector2 locatorBox; //Will only be used for box movement

				if (IsKeyPressed(left))
				{
					switchState(state::IDLE_LEFT);
					locatorCharacter.y = (float)position.y;
					locatorCharacter.x = (float)position.x - 1;

					if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getActive())
					{
						//Update player state
						switchState(state::WALKING_LEFT);

						//Update the player�s position
						position.x--;

						//Blocks player from moving while animation plays
						_moving = true;

						//Set target for movement
						target.x = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().x;
						target.y = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().y;
						if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getBox())
						{
							int size = gameplay::levelBoxes.size();

							locatorBox.y = (float)locatorCharacter.y;
							locatorBox.x = (float)locatorCharacter.x - 1;

							for (short i = 0; i < size; i++)
							{
								if ((int)gameplay::levelBoxes[i]->getPosition().y == (int)locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == (int)locatorCharacter.x)
								{
									gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->moveBox();
									gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->placeBox();

									int size = gameplay::levelBoxes.size();
									for (short i = 0; i < size; i++)
									{
										if (gameplay::levelBoxes[i]->getPosition().y == locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == locatorCharacter.x)
										{
											gameplay::levelBoxes[i]->setPosition(locatorBox);
										}
									}

									gameplay::levelBoxes[i]->setTarget(gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->getHitbox());
									gameplay::levelBoxes[i]->setState(boxes::state::MOVING_LEFT);				
									break;
								}
							}
						}
					}
				}
				if (IsKeyPressed(right))
				{
					switchState(state::IDLE_RIGHT);
					locatorCharacter.y = (float)position.y;
					locatorCharacter.x = (float)position.x + 1;

					if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getActive())
					{
						//Update player state
						switchState(state::WALKING_RIGHT);

						//Update the player�s position
						position.x++;

						//Blocks player from moving while animation plays
						_moving = true;

						//Set target for movement
						target.x = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().x;
						target.y = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().y;

						if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getBox())
						{
							int size = gameplay::levelBoxes.size();

							locatorBox.y = (float)locatorCharacter.y;
							locatorBox.x = (float)locatorCharacter.x + 1;

							for (short i = 0; i < size; i++)
							{
								if ((int)gameplay::levelBoxes[i]->getPosition().y == (int)locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == (int)locatorCharacter.x)
								{
									gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->moveBox();
									gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->placeBox();

									int size = gameplay::levelBoxes.size();
									for (short i = 0; i < size; i++)
									{
										if (gameplay::levelBoxes[i]->getPosition().y == locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == locatorCharacter.x)
										{
											gameplay::levelBoxes[i]->setPosition(locatorBox);
										}
									}

									gameplay::levelBoxes[i]->setTarget(gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->getHitbox());
									gameplay::levelBoxes[i]->setState(boxes::state::MOVING_RIGHT);
									break;
								}
							}
						}
					}
				}
				if (IsKeyPressed(up))
				{
					switchState(state::IDLE_UP);
					locatorCharacter.y = (float)position.y - 1;
					locatorCharacter.x = (float)position.x;

					if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getActive())
					{
						//Update player state
						switchState(state::WALKING_UP);

						//Update the player�s position
						position.y--;

						//Blocks player from moving while animation plays
						_moving = true;

						//Set target for movement
						target.x = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().x;
						target.y = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().y;

						if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getBox())
						{
							int size = gameplay::levelBoxes.size();

							locatorBox.y = (float)locatorCharacter.y - 1;
							locatorBox.x = (float)locatorCharacter.x;

							for (short i = 0; i < size; i++)
							{
								if ((int)gameplay::levelBoxes[i]->getPosition().y == (int)locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == (int)locatorCharacter.x)
								{
									gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->moveBox();
									gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->placeBox();

									int size = gameplay::levelBoxes.size();
									for (short i = 0; i < size; i++)
									{
										if (gameplay::levelBoxes[i]->getPosition().y == locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == locatorCharacter.x)
										{
											gameplay::levelBoxes[i]->setPosition(locatorBox);
										}
									}

									gameplay::levelBoxes[i]->setTarget(gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->getHitbox());
									gameplay::levelBoxes[i]->setState(boxes::state::MOVING_UP);
									break;
								}
							}
						}
					}
				}
				if (IsKeyPressed(down))
				{
					switchState(state::IDLE_DOWN);
					locatorCharacter.y = (float)position.y + 1;
					locatorCharacter.x = (float)position.x;

					if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getActive())
					{
						//Update player state
						switchState(state::WALKING_DOWN);

						//Update the player�s position
						position.y++;

						//Blocks player from moving while animation plays
						_moving = true;

						//Set target for movement
						target.x = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().x;
						target.y = gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getHitbox().y;

						if (gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->getBox())
						{
							int size = gameplay::levelBoxes.size();

							locatorBox.y = (float)locatorCharacter.y + 1;
							locatorBox.x = (float)locatorCharacter.x;

							for (short i = 0; i < size; i++)
							{
								if ((int)gameplay::levelBoxes[i]->getPosition().y == (int)locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == (int)locatorCharacter.x)
								{
									gameplay::blankSlate[(int)locatorCharacter.y][(int)locatorCharacter.x]->moveBox();
									gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->placeBox();

									int size = gameplay::levelBoxes.size();
									for (short i = 0; i < size; i++)
									{
										if (gameplay::levelBoxes[i]->getPosition().y == locatorCharacter.y && (int)gameplay::levelBoxes[i]->getPosition().x == locatorCharacter.x)
										{
											gameplay::levelBoxes[i]->setPosition(locatorBox);
										}
									}

									gameplay::levelBoxes[i]->setTarget(gameplay::blankSlate[(int)locatorBox.y][(int)locatorBox.x]->getHitbox());
									gameplay::levelBoxes[i]->setState(boxes::state::MOVING_DOWN);
									break;
								}
							}
						}

					}
				}
			}
		}

		void player::draw()
		{
			DrawTexturePro(textures::player, _sourceRec, _hitbox, { 0,0 }, 0, WHITE);
		}

		Vector2 player::getPosition()
		{
			return position;
		}

		void player::setHitbox(Rectangle hitbox)
		{
			_hitbox = hitbox;
		}

		void player::setPosition(int y, int x)
		{
			position.y = (float)y;
			position.x = (float)x;
		}
	}
}