#include "Objects/Tiles/Tiles.h"

namespace sokovan
{
	namespace tiles
	{
		tiles::tiles()
		{
			_active = false;
			_hitbox = { 0,0,0,0 };
			_box = false;
			_destination = false;
		}

		void tiles::activate()
		{
			_active = true;
		}

		void tiles::setHitbox(Rectangle newHitbox)
		{
			_hitbox = newHitbox;
		}

		void tiles::draw()
		{
			if (_destination)
			{
				DrawTexture(textures::floor, (int)_hitbox.x, (int)_hitbox.y, GREEN);
			}
			else
			{
				DrawTexture(textures::floor, (int)_hitbox.x, (int)_hitbox.y, WHITE);
			}
		}

		bool tiles::getActive()
		{
			return _active;
		}

		void tiles::destination()
		{
			_destination = true;
		}

		bool tiles::getDestination()
		{
			return _destination;
		}

		Rectangle tiles::getHitbox()
		{
			return _hitbox;
		}

		void tiles::placeBox()
		{
			_box = true;
		}

		bool tiles::getBox()
		{
			return _box;
		}

		void tiles::moveBox()
		{
			_box = false;
		}
	}
}