#ifndef TILES_H
#define TILES_H

#include "raylib.h"
#include "Textures & Sounds/Textures_Sounds.h"

namespace sokovan
{
	namespace tiles
	{
		class tiles
		{
		private:
			bool _active;
			bool _destination;
			bool _box;
			Rectangle _hitbox;
		public:
			tiles();
			void activate();
			void setHitbox(Rectangle newHitbox);
			void draw();
			bool getActive();
			void destination();
			bool getDestination();
			Rectangle getHitbox();
			void placeBox();
			bool getBox();
			void moveBox();
		};
	}
}

#endif // !TILES_H

