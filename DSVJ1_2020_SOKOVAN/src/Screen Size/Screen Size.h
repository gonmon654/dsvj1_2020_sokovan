#ifndef  SCREEN_SIZE_H
#define SCREEN_SIZE_H

namespace sokovan
{
	namespace screen_size
	{
		const int screenWidth = 1024;
		const int screenHeight = 768;
	}
}

#endif // ! SCREEN_SIZE_H

